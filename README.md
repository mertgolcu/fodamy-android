# Fodamy
### Android ZoneZero Yemek tarifi projesi 
<br/>


#### Gereksinimler

- API21+
- MVVM
- SOLID
- [Ktlint](https://ktlint.github.io/)
- [Dagger Hilt](https://dagger.dev/hilt/)
- [Retrofit](https://square.github.io/retrofit/)
- [Gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)
- [Jetpack Components](https://developer.android.com/jetpack)
- [Postman](https://www.postman.com/)

<hr/>

## dependencies
```
    // Navigation Component
    implementation "androidx.navigation:navigation-fragment-ktx:2.3.4"
    implementation "androidx.navigation:navigation-ui-ktx:2.3.4"
    implementation "androidx.navigation:navigation-dynamic-features-fragment:2.3.4"

    // Dagger Hilt
    implementation "com.google.dagger:hilt-android:2.33-beta"
    kapt "com.google.dagger:hilt-android-compiler:2.33-beta"
    implementation "androidx.hilt:hilt-lifecycle-viewmodel:1.0.0-alpha03"
    kapt "androidx.hilt:hilt-compiler:1.0.0-alpha03"

    // Retrofit + GSON
    implementation "com.squareup.retrofit2:retrofit:2.9.0"
    implementation "com.squareup.retrofit2:converter-gson:2.9.0"

    // Paging 3
    implementation "androidx.paging:paging-runtime:3.0.0-alpha05"

    // DataStore
    implementation "androidx.datastore:datastore-preferences:1.0.0-alpha08"

```
<hr>
